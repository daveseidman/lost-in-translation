import React, { useEffect, useState } from 'react';
import Styles from './App.scss'; // eslint-disable-line
import Home from './views/home';
import { translateAPI } from './utils';

export default function App() {
  const [languages, setLanguages] = useState([]);

  useEffect(() => {
    console.log('get languages');
    fetch(`${translateAPI}/languages`).then((res) => res.json()).then((res) => {
      setLanguages(res);
    });
  }, []);

  return (
    <div className="app">
      <h1>Lost in Translation</h1>
      <Home languages={languages} />
    </div>
  );
}
